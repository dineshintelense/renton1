// src/views/Home.js
import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { withNavigation } from "react-navigation";
import Product from "../components/Product";

class HomeScreen extends React.Component {
  render() {
    return (
      <ScrollView
        style={{
          flexGrow: 0,
          width: "100%",
          height: "100%"
        }}
      >
        <View style={styles.row}>
          <View style={styles.col}>
            <Product />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center"
  },
  col: {
    flexDirection: "column",
    flex: 2
  }
});

export default withNavigation(HomeScreen);
