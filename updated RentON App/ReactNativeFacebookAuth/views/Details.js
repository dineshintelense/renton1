import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { withNavigation } from "react-navigation";
import * as Facebook from "expo-facebook";
import { IOS_CLIENT_ID, AND_CLIENT_ID } from "react-native-dotenv";
import { FontAwesome } from "@expo/vector-icons";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { phoneDetails } from "../components/phoneDetails";

import { Ionicons } from "@expo/vector-icons";
function Details() {
  const [isLogged, setLoggedStatus] = useState(false);
  const [userData, setUserData] = useState(null);
  const [isImageLoading, setImageLoadStatus] = useState(false);

  let logout;

  async function facebookLogIn() {
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions
      } = await Facebook.logInWithReadPermissionsAsync("372590507213652", {
        permissions: ["public_profile"]
      });
      if (type === "success") {
        // Get the user's name using Facebook's Graph API
        fetch(
          `https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.height(500)`
        )
          .then(response => response.json())
          .then(data => {
            setLoggedStatus(true);
            setUserData(data);
          })
          .catch(e => console.log(e));
      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }
  const signing = () => {
    facebookLogIn();
  };

  async function signInWithGoogleAsync() {
    try {
      const result = await Google.logInAsync({
        behavior: "web",
        iosClientId: IOS_CLIENT_ID,
        //androidClientId: AND_CLIENT_ID,
        scopes: ["profile", "email"]
      });

      if (result.type === "success") {
        return result.accessToken;
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      alert(`Google Login Error: ${e}`);
    }
  }
  const signInWithGoogle = () => {
    signInWithGoogleAsync();
  };
  logout = () => {
    setLoggedStatus(false);
    setUserData(null);
    setImageLoadStatus(false);
  };

  return isLogged ? (
    userData ? (
      <View style={styles.container}>
        <Image
          style={{ width: 200, height: 200, borderRadius: 50 }}
          source={{ uri: userData.picture.data.url }}
          onLoadEnd={() => setImageLoadStatus(true)}
        />
        <ActivityIndicator
          size="large"
          color="#0000ff"
          animating={!isImageLoading}
          style={{ position: "absolute" }}
        />
        <Text style={{ fontSize: 22, marginVertical: 10 }}>
          Hi {userData.name}!
        </Text>
        <TouchableOpacity style={styles.logoutBtn} onPress={this.logout}>
          <Text style={{ color: "#fff" }}>Logout</Text>
        </TouchableOpacity>
      </View>
    ) : null
  ) : (
    <View style={styles.login}>
      <FontAwesome.Button
        name="facebook"
        backgroundColor="#3b5998"
        borderRadius={20}
        onPress={() => signing()}
      >
        Facebook
      </FontAwesome.Button>
      <FontAwesome.Button
        name="google"
        borderRadius={20}
        backgroundColor="white"
        color="black"
        type="google"
        borderColor="grey"
        onPress={() => signInWithGoogle()}
      >
        Google
      </FontAwesome.Button>
    </View>
  );
}
class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.Text}>RENTON</Text>
          <TouchableOpacity
            style={styles.TextInputStyleClass}
            onPress={() => this.props.navigation.navigate("phoneDetails")}
          >
            <Text style={styles.Numbertext}>
              <Ionicons
                name="ios-keypad"
                size={24}
                color="grey"
                paddingHorizontal={10}
              />
              {"         "}Mobile Number
            </Text>
          </TouchableOpacity>
          <View>
            <Details />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    borderRadius: 12,
    flex: 1,
    flexDirection: "row",

    alignItems: "center",
    justifyContent: "center"
  },

  TextInputStyleClass: {
    textAlign: "center",
    height: 50,
    borderWidth: 2,
    borderColor: "grey",
    borderRadius: 30,
    backgroundColor: "#FFFFFF",
    top: 70,
    margin: 60
  },
  Text: {
    fontWeight: "bold",
    fontSize: 70,
    alignContent: "center",
    margin: 10,
    color: "purple",
    fontFamily: "Sarpanch-SemiBold",
    textAlign: "center",
    top: 200
  },
  button: {
    alignItems: "center",
    textAlign: "center",
    height: 40
  },
  button1: {
    alignItems: "center",
    textAlign: "center",
    height: 40
  },
  Numbertext: {
    alignContent: "center",
    margin: 10,
    width: 250,
    alignItems: "center",
    textAlign: "center",
    fontSize: 17,
    paddingRight: 40
  },
  login: {
    borderRadius: 12,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",

    paddingHorizontal: 70,
    paddingVertical: 50,
    height: 10
  }
});

export default withNavigation(DetailsScreen);
